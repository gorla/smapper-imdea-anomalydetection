import ConfigParser
import logging
import json
import os
import subprocess

logging.basicConfig(filename='smapper-rest.log',level=logging.INFO)

## Config file parsing to get values
Config = ConfigParser.ConfigParser()
Config.read('luigi.cfg')
descr_folder = Config.get('ConvertFilteredDescriptionToTXTFromFolder', 'json_input_folder')
knn_folder = Config.get('KNN', 'knn_output_folder')
mallet_folder = Config.get('Mallet', 'output_folder')

'''Given a pkgname, the function returns the anomaly score and the list of
anomalies. Returns None if the pkgname cannot be found

'''
def get_anomalies_for_pkg(sample):
    new_dic = {}
    new_dic['pkg_name'] = sample['PackageName']
    new_dic['Details'] = {}
    new_dic['Details']['permissions'] = sample['Details']['Permissions']
    new_dic['Details']['description'] = sample['Details']['Description']
    new_dic['vercode'] = sample['Version']

    pkg_name = sample['PackageName']
    for i in os.listdir('knn'):
        with open(os.path.join(knn_folder, i), 'r') as k:
            a = json.loads(k.read())
            if pkg_name not in a.keys():               
                continue
            logging.info('App '+pkg_name+' has been analyzed.')
            assert(not a[pkg_name] == None) 
            return a[pkg_name]

    # the app has not been analyzed yet, launch the inference
    logging.info('App '+pkg_name+' not yet analyzed. Launching inference.')
    json_file = os.path.join(descr_folder, pkg_name+'-details.json')
    with open(json_file, 'w') as fout:
        json.dump(new_dic, fout)

    subprocess.call(['python2.7','-m', 'luigi', '--module',
                     'chabada_analysis', 'AnalysisManager',
                     '--sample-file', json_file] )
    return get_anomalies_for_pkg(sample)

'''Given a pkgname, the function returns a string representing the
category name (which can be made by one or more topics).

'''
def get_type_app(sample):
    pkg_name = sample['PackageName']
    with open('category-names.json', 'r') as cat_file:
        cat_file_content = json.loads(cat_file.read())
    with open(os.path.join(mallet_folder,
                           'apps_list_with_topics.json') , 'r') as a:
            content = json.loads(a.read())
            cat_name = ''
            for key in content[pkg_name].keys():
                cat_name = cat_name + cat_file_content[key] + ','
            return cat_name[:-1] # remove last
