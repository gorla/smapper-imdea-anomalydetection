import json
import luigi
import os

from kmeans import KmeansPrediction
from knn import KNN

'''Main task to analyze a new sample model. This will filter
description from the JSON file provided as input, will infer topics,
assign the sample to an existing cluster, and rerun knn for that
specific cluster.

'''
class AnalysisManager(luigi.WrapperTask):

    sample_file = luigi.Parameter()
    clusters_folder = luigi.Parameter()
    knn_folder = luigi.Parameter()

    def requires(self):
        return KmeansPrediction(input_file=self.sample_file)
    
    def complete(self):
        # no need to run if the file has been analyzed already
        app_name = os.path.basename(self.sample_file.replace('-details.json', ''))
        for knn_file in os.listdir(self.knn_folder):
            with open(os.path.join(self.knn_folder, knn_file), 'r') as f:
                if app_name in f.read():
                    return True
        return False

    def run(self):

        # This check is necessary since yielding the knn task will
        # force run to restart from the beginning
        if self.complete():
            return

        # read file produced by kmeans to see which cluster the sample
        # has been assigned to.
        with open(os.path.join(self.clusters_folder,
                               os.path.basename(self.sample_file)),
                  'r') as app_cluster_id_file:

            app_cluster_id = json.loads(app_cluster_id_file.read())
            cluster_id = str(app_cluster_id.values()[0])

            # remove cluster_id.txt and knn_id.txt files so knn task
            # can generate them again
            cf_name = os.path.join(self.clusters_folder,
                                   'cluster_'+cluster_id+'.txt')
            kf_name = os.path.join(self.knn_folder,
                                   cluster_id+'_knn.txt')
            if os.path.exists(cf_name):
                os.remove(cf_name)
            if os.path.exists(kf_name):
                os.remove(kf_name)

            # recompute anomaly score for all samples in that cluster
            yield KNN(cluster_id=cluster_id)
