import cPickle as pickle
import luigi
import numpy
import json
import os
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score

from mallet import Mallet
from mallet import MalletInferencer

'''This task takes as an input a file in Mallet output format and
assigns each sample to a cluster.

'''
class KmeansTraining (luigi.Task):
    
    num_clusters = luigi.IntParameter()
    num_topics = luigi.IntParameter()
    output_folder = luigi.Parameter()

    # Filename to store clustering results
    kmeans_output = luigi.Parameter(default='clusters.json')
    # Filename to store the kmeans model (may be used later to predict
    # new samples)
    model_file = luigi.Parameter(default='kmeans.model')

    silhouette_out = luigi.Parameter(default=None)

    def requires(self):
        return Mallet()

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_folder,
                                              self.kmeans_output))

    def run(self):
        # Build matrix from input file
        with self.input().open('r') as f:
            apps_list_with_topics = json.loads(f.read())

        # Initialize the input matrix to use in kmeans
        num_samples = len(apps_list_with_topics)
        probs_matrix = numpy.zeros((num_samples, self.num_topics))

        # Fill the input matrix with data from file input file
        for i, app in enumerate(apps_list_with_topics):
            # tuple [topic, probability]
            for tp in apps_list_with_topics[app]:
                probs_matrix[i-1][int(tp)] = float(apps_list_with_topics[app][tp])

        # Apply KMeans clustering to probs_matrix, with the same seed
        model = KMeans(n_clusters = self.num_clusters, random_state =
                       1).fit(probs_matrix)

        # if output file path does not exist create it.
        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

        # Save the model to the output folder
        pickle.dump(model, open(os.path.join(self.output_folder,
                                             self.model_file), 'wb'))

        cluster_labels = model.predict(probs_matrix)

        clusters_dict={}
        for i, cl in enumerate(cluster_labels):
            if not clusters_dict.has_key(str(cl)):
                clusters_dict[str(cl)] = []
            list_apps = clusters_dict[str(cl)]
            list_apps.append(apps_list_with_topics.keys()[i])
            clusters_dict[str(cl)] = list_apps

        with self.output().open('wb') as f:
            f.write(json.dumps(clusters_dict))

        # Silhouette
        if not self.silhouette_out == None:
            silhouette_avg = silhouette_score(probs_matrix, cluster_labels)
            with open(self.silhouette_out, 'w') as fsil:
                fsil.write('%s SilhouetteScore: %.4f Topics: %d Clusters: %d \n' % (self.silhouette_out, silhouette_avg, self.num_topics,
                                 self.num_clusters))


'''This task assigns a new sample to a cluster, given a trained
k-means model.

'''
class KmeansPrediction (luigi.Task):

    num_topics = luigi.IntParameter()
    input_file = luigi.Parameter()
    output_folder = luigi.Parameter()

    # Filename with trained kmeans model
    model_file = luigi.Parameter(default='kmeans.model')
    # Filename to store clustering results
    kmeans_output = luigi.Parameter(default='clusters.json')

    def requires(self):
        return MalletInferencer(input_file=self.input_file)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_folder,
                                              self.kmeans_output))
    
    def complete(self):
        app_name = os.path.basename(self.input_file.replace('-details.json', ''))
        with open(os.path.join(self.output_folder,
                               self.kmeans_output), 'r') as f:
            if app_name in f.read():
                return True
            else:
                return False

    def run(self):
        # Build matrix from input file
        with self.input().open('r') as f:
            app_list_with_topics = json.loads(f.read())

        sample_app_name = os.path.basename(self.input_file.replace('-details.json', ''))

        # Initialize the input matrix to use in kmeans
        probs_vector = numpy.zeros(self.num_topics)

        # Fill the vector with data from input file
        # tuple [topic, probability]
        for tp in app_list_with_topics[sample_app_name]:
            probs_vector[int(tp)] = float(app_list_with_topics[sample_app_name][tp])
        
        # Import kmeans model
        with open(os.path.join(self.output_folder, self.model_file),
                  'rb') as fd:
            kmeans_model = pickle.loads(fd.read())

        cluster = int(kmeans_model.predict(probs_vector.reshape(1,-1))[0])

        # append new sample to cluster file
        with open(os.path.join(self.output_folder,
                               self.kmeans_output), 'r+') as f:
            clusters_dict = json.loads(f.read())
            list_apps = clusters_dict[str(cluster)]
            
            list_apps.append(sample_app_name)
            clusters_dict[str(cluster)] = list_apps
            assert(sample_app_name in clusters_dict[str(cluster)])
            f.seek(0)
            f.write(json.dumps(clusters_dict))
            f.truncate()

        # write out appname and cluster id
        with open(os.path.join(self.output_folder,
                               os.path.basename(self.input_file)),
                  'w') as file_out:
            kmeans_dict_out = {}
            kmeans_dict_out[sample_app_name] = cluster
            json.dump(kmeans_dict_out, file_out)
