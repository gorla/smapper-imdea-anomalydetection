import fnmatch
import json
import luigi
import os

from preprocess import Filters

'''This task takes a JSON file as input, looks for the filtered
description key and writes it to a file in in output_folder.

'''
class ConvertFilteredDescriptionToTXT(luigi.Task):

    output_folder = luigi.Parameter()
    json_file = luigi.Parameter()
    
    def requires(self):
        return Filters(json_file = self.json_file)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_folder,
                                              os.path.basename(self.json_file)))

    def run(self):
        # Read JSON content from input
        with self.input().open('r') as f:
            app = json.loads(f.read())
            f_descr = app['filtered_descr']

            # Here we write the filtered description to the out file
            # check if output folder exists. If no create it
            if not os.path.exists(self.output_folder):
                os.mkdir(self.output_folder)

            # write to out file
            with self.output().open('w') as f:
                f.write(f_descr.encode('ascii', 'ignore'))


'''This task calls ConvertFilteredDescriptionToTXT for all the json
files in a folder provided as parameter.

'''
class ConvertFilteredDescriptionToTXTFromFolder(luigi.WrapperTask):

    json_input_folder = luigi.Parameter()
    output_folder = luigi.Parameter()

    def requires(self):
        json_files = fnmatch.filter(os.listdir
                                    (self.json_input_folder),
                                    '*.json')
        return [ConvertFilteredDescriptionToTXT(
            output_folder=self.output_folder,
            json_file=os.path.join(self.json_input_folder, ff)) for ff
                in json_files]
