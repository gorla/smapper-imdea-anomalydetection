import fnmatch
import json
import logging
import luigi
import os

logger = logging.getLogger('luigi-interface')

"""This task checks whether the input folder contains valid JSON
files. It will move invalid files to the output_invalid_folder
provided as a parameter.

"""
class RemoveInvalidJSONsFromFolderTask(luigi.Task):
    
    input_folder = luigi.Parameter()
    output_invalid_folder = luigi.Parameter()

    def run(self):
        # Create out directory if it does not exist
        if not os.path.exists(self.output_invalid_folder):
            os.makedirs(self.output_invalid_folder)

        # Try to load every JSON file in input folder.
        # Move the ones that do not load. 
        json_files = fnmatch.filter(os.listdir(self.input_folder),
                                    '*.json')
        for j in json_files:
            file_path = os.path.join(self.input_folder, j)
            f = open(file_path, 'rb')
            try:
                json.loads(f.read())
            except:
                logger.info(j + ' is an invalid JSON file.')
                os.rename(file_path,
                          os.path.join(self.output_invalid_folder, j))
            finally:
                f.close()

