import json
import logging
import luigi
import numpy as np
import os
from sklearn.neighbors import NearestNeighbors
from sklearn.metrics.pairwise import pairwise_distances

from build_cluster_files import BuildClusterFile

logger = logging.getLogger('luigi-interface')

'''Computes the K nearest neighbors for a single cluster_id. This task
will produce the anomaly scores for each sample in the cluster and
will provide the anomaly score for each feature of the sample.

'''
class KNN(luigi.Task):
    
    cluster_id = luigi.Parameter()
    cluster_files_folder = luigi.Parameter()
    knn_output_folder = luigi.Parameter()
    n_neighbors = luigi.IntParameter(default=3)


    def requires(self):
        return BuildClusterFile(cluster_id = self.cluster_id)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.knn_output_folder,
                                              self.cluster_id)+'_knn.txt')

    def run(self):
        
        # Initilization
        perms_dict = {}
        app_index = {}
        perm_index = {}
        
        # Parse the file cluster_x.txt. Put apps with perms in a dictionary
        with open('%s/cluster_%s.txt' % (self.cluster_files_folder,
                                         self.cluster_id), 'r') as cluster_samples:
            for line in cluster_samples:
                app_name = line.split()[0]

                # len(app_index) is basically a counter starting from 0
                app_index[app_name] = len(app_index)
                app_perms = line.strip().split()[1:]

                # Build dict of permission -> [app1, app2,...]
                for perm in app_perms:
                    if perm not in perms_dict:
                        perms_dict[perm] = [app_name]
                    else:
                        perms_dict[perm].append(app_name)

        # Initialize the permissions matrix to all zeros
        knn_matrix = np.zeros((len(app_index), len(perms_dict)))

        # Fill the matrix with 1s where necessary
        counter = 0
        for p in perms_dict:
            app_list = perms_dict[p]
            perm_index[p] = counter
            for app in app_list:
                knn_matrix[app_index[app]][perm_index[p]] = 1
            counter += 1

        # List of all permissions
        perm_list=perm_index.keys()
        
        # First get the row number for each sample in a list
        apps_list = app_index.keys()
        # We get the rows from the matrix
        row_numbers = [app_index[app] for app in apps_list]

        # not enough samples -> skip
        if len(row_numbers) < self.n_neighbors:
            logger.error('Not enough samples in cluster. Aborting...')
            return

        # C_matrix has the permissions for each application
        c_matrix = knn_matrix[row_numbers, :]

        # nbrs builds the model for knn
        nbrs = NearestNeighbors(n_neighbors = self.n_neighbors,
                                algorithm='brute').fit(c_matrix)

        # Distances has dist from each point to its K nearest neighbors, for every point in cluster
        distances, indices = nbrs.kneighbors(c_matrix)

        # It is -1 because we skip the point with distance 0 to itself FIXME: can we avoid this? it is only for training!
        avg_dist_vector = distances.sum(axis=1) / (self.n_neighbors -1)

        # We want to assign each app name to an avg_distance
        # Scores has app_name -> avg_dist to its k-neighbors
        scores = {}
        for i in range(len(apps_list)):
            scores[apps_list[i]] = {}
            scores[apps_list[i]]['score'] = avg_dist_vector[i]

        # calculate feature importance
        for app_idx, app in enumerate(indices):
            sample_vector = c_matrix[app_idx,:]
            neighbors_matrix = c_matrix[app,:]
            feature_importance={}
            for feature_idx in range(len(perm_index)):
                feature_excl_dist = pairwise_distances(np.delete(sample_vector,feature_idx).reshape(1, -1), 
                                                       np.delete(neighbors_matrix,feature_idx,axis=1))
                avg_feature_dist = np.sum(feature_excl_dist) / (self.n_neighbors-1)
                if avg_feature_dist<avg_dist_vector[app_idx]:
                    feature_importance_score=avg_dist_vector[app_idx]-avg_feature_dist
                    feature_importance[perm_list[feature_idx]]=feature_importance_score
            # save
            scores[apps_list[app_idx]]['features']=feature_importance

        #with open('importance.txt', 'w') as imp_file:
        with self.output().open('w') as imp_file:
            json.dump(scores, imp_file)
