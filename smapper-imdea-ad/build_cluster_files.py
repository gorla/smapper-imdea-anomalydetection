import json
import luigi
import os

from kmeans import KmeansTraining
from smapper_json import LoadSmapperJSONFile

'''Reads the file listing all apps and cluster ids produced by Kmeans
and builds a new file for a specific cluster_id listing all apps and
all their features (i.e. permissions). The file will be named
cluster_XX where XX is the cluster id, and placed in the
output_folder.

'''
class BuildClusterFile(luigi.Task):

    cluster_id = luigi.Parameter()
    json_input_folder = luigi.Parameter()
    output_folder = luigi.Parameter()

    def requires(self):
        return KmeansTraining()

    def output(self):
        return luigi.LocalTarget('%s/cluster_%s.txt' %
                                 (self.output_folder,
                                  self.cluster_id))

    def run(self):
        with self.input().open('r') as clusters_fd:
            cluster_id_list_with_apps = json.loads(clusters_fd.read())
            list_class_for_cluster = cluster_id_list_with_apps[self.cluster_id]

            # check if output folder exists. Create it otherwise.
            if not os.path.exists(self.output_folder):
                os.makedirs(self.output_folder)

            with self.output().open('w') as out_fd:
                for app in list_class_for_cluster:
                    #FIXME: Watch out! We assume the file is named as
                    #follows. Easy to break!
                    app_json = yield LoadSmapperJSONFile(
                        json_file=self.json_input_folder +'/'+ app +
                        '-details.json')
                    with app_json.open('r') as f:
                        app = json.loads(f.read())
                        out_fd.write(app['pkg_name'] + ' ' +
                                     app['permissions'].replace(',',' ') + '\n')
