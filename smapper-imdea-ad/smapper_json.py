import luigi
from luigi.mock import MockFile
import json

'''Represents a JSON file for the Luigi pipeline.

'''

class JSONFile(luigi.ExternalTask):
    json_file = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(self.json_file)


'''LoadSmapperJSONFile opens a JSON file and loads its content in
memory to start the Luigi pipeline. It produces a JSON content in the
format:

app{
  "pkg_name" = ..
  "version" = ..
  "description" = ..
  "permissions" = [..]
}

'''
class LoadSmapperJSONFile(luigi.Task):

    json_file = luigi.Parameter()

    def requires(self):
        return JSONFile(json_file=self.json_file)

    def output(self):
        return MockFile('smapper' + self.json_file, mirror_on_stderr=False)
       # return luigi.LocalTarget('tt/smapper' + self.json_file)

    def run(self):
        app = {}
        with self.input().open('r') as f:
            details = json.loads(f.read())
            app['pkg_name'] = details['pkg_name']
            app['permissions'] = details['Details']['permissions']  # Permissions
            app['description'] = details['Details']['description']  # Description
            app['version'] = details['vercode']

        f = self.output().open('w')
        f.write(json.dumps(app))
        f.close()

        
'''LoadSmapperFEJSONFile opens a JSON file and loads its content in
memory to start the Luigi pipeline. It produces a JSON content in the
format:

app{
  "pkg_name" = ..
  "version" = ..
  "description" = ..
  "permissions" = [..]
}x

'''
class LoadSmapperFEJSONFile(luigi.Task):

    json_file = luigi.Parameter()

    def requires(self):
        return JSONFile(json_file=self.json_file)

    def output(self):
        return MockFile('smapper' + self.json_file, mirror_on_stderr=False)
       # return luigi.LocalTarget('tt/smapper' + self.json_file)

    def run(self):
        app = {}
        with self.input().open('r') as f:
            details = json.loads(f.read())
            app['pkg_name'] = details['PackageName']
            app['permissions'] = details['Details']['Permissions']
            app['description'] = details['Details']['Description']
            app['version'] = details['Version']
            app['hash'] = details['Hash']

        f = self.output().open('w')
        f.write(json.dumps(app))
        f.close()
