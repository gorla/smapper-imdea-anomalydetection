import re
import string
import enchant
import json
import logging
import luigi
from luigi.mock import MockFile
import nltk.stem.wordnet as wordnet
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import SnowballStemmer
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tokenize import word_tokenize
import os

from smapper_json import LoadSmapperJSONFile
from smapper_json import LoadSmapperFEJSONFile

logger = logging.getLogger('luigi-interface')

'''Applies several filters to the input text

'''
class Filters(luigi.Task):

    html = luigi.BoolParameter(default=True)
    numbers = luigi.BoolParameter(default=True)
    lowercase = luigi.BoolParameter(default=True)
    non_ascii = luigi.BoolParameter(default=True)
    punctuation = luigi.BoolParameter(default=True)
    non_english = luigi.BoolParameter(default=True)
    stop_words = luigi.BoolParameter(default=True)
    stemming = luigi.BoolParameter(default=True)
    lemmatization = luigi.BoolParameter(default=True)

    json_file = luigi.Parameter()

    @staticmethod
    def remove_html(text):
        '''Remove all html tags from text'''
        clean = re.compile('<.*?>')
        return re.sub(clean, '', text)

    @staticmethod
    def remove_non_english(text):
        '''Remove all words that are not in the US+GB dictionary'''
        d_us = enchant.Dict("en_US")
        d_gb = enchant.Dict("en_GB")

        # build custom dict
        alt_dicts = []
        for d_file in os.listdir('no-filter-dict'):
            with open(os.path.join('no-filter-dict', d_file), 'r') as ff:
                list_terms = ff.read().lower().split()
                alt_dicts.extend(list_terms)
        new_text = ''
        for t in text.split():
            # if text is eith in US or GB english dict keep it
            if d_us.check(t) or d_gb.check(t):
                new_text = new_text + t + " "
                continue
            if t in alt_dicts:
                new_text = new_text + t + " "
                continue
            logger.debug('Removing ' + t)
        return new_text

    @staticmethod
    def remove_non_ascii(text):
        '''Replace non ascii chars with spaces'''
        printable = set(string.printable)
        return filter(lambda x: x in printable, text)

    @staticmethod
    def remove_punctuation(text):
        '''Replace punctuation chars '!"#$%&\'()*+,-./:;<=>?@[\\]^_`{|}~' with spaces'''
        return ''.join(map(lambda c: ' ' if c in string.punctuation else c, text))

    @staticmethod
    def remove_stopwords(text):
        '''Remove words with no meaning or irrelevant for searching'''
        return ' '.join([s for s in text.split() if s not in
                         stopwords.words('english')])

    @staticmethod
    def apply_stemming(text):
        '''Extracts the root of every word'''
        stemmer = SnowballStemmer("english")
        tokens = word_tokenize(text)
        stemmed_tokens = map(stemmer.stem, tokens)
        return ' '.join(stemmed_tokens)

    @staticmethod
    def wordnet_pos_code(tag):
        if tag.startswith('NN'):
            return wordnet.wordnet.NOUN
        elif tag.startswith('VB'):
            return wordnet.wordnet.VERB
        elif tag.startswith('JJ'):
            return wordnet.wordnet.ADJ
        elif tag.startswith('RB'):
            return wordnet.wordnet.ADV
        else:
            return wordnet.NOUN  # default value

    @staticmethod
    def apply_lemmatization(text):
        lmtzr = WordNetLemmatizer()
        tokens = word_tokenize(text)
        pos_tokens = pos_tag(tokens)
        lemm_tokens = []
        for token, pos in pos_tokens:
            w = lmtzr.lemmatize(token, Filters.wordnet_pos_code(pos))
            lemm_tokens.append(w)
        return ' '.join(lemm_tokens)

    @staticmethod
    def remove_numbers(text):
        '''Remove numbers from text'''
        clean = re.compile('[0-9]')
        return re.sub(clean, '', text)

    @staticmethod
    def do_lowercase(text):
        '''Text to lowercase, for stopwords and stuff'''
        return text.lower()

    
    def requires(self):
        with open(self.json_file, 'r') as f:
            f_cont = json.loads(f.read())
            if 'pkg_name' in f_cont.keys():
                return LoadSmapperJSONFile(json_file=self.json_file)
            else:
                return LoadSmapperFEJSONFile(json_file=self.json_file)

    def output(self):
        return MockFile('filtered_'+self.json_file, mirror_on_stderr=False)
#        return luigi.LocalTarget('tt/filtered_'+self.json_file)

    def run(self):
        # Read JSON content from input
        with self.input().open('r') as f:
            app = json.loads(f.read())        
           
        # Here we apply filters to info.description
        text = app['description']
        if self.html:
            text = self.remove_html(text)
        if self.non_ascii:
            text = self.remove_non_ascii(text)
        if self.punctuation:
            text = self.remove_punctuation(text)
        if self.lowercase:
            text = self.do_lowercase(text)
        if self.numbers:
            text = self.remove_numbers(text)
        if self.stop_words:
            text = self.remove_stopwords(text)
        if self.non_english:
            text = self.remove_non_english(text)
        if self.lemmatization:
            text = self.apply_lemmatization(text)
        if self.stemming:
            text = self.apply_stemming(text)

        # We add the filtered description to the JSON content and
        # write it to output
        app['filtered_descr'] = text
        with self.output().open('w') as f:
            f.write(json.dumps(app))
