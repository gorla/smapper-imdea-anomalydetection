import luigi
from knn import KNN

'''Main task to train the model. This will extract descriptions from
all JSON files (input directory specified in the luigi.conf file),
will preprocess them (applying different filters), will run topic
modeling, k-means and finally knn to detect anomalous permissions on
all clusters.

'''
class TrainingManager(luigi.WrapperTask):

    num_clusters = luigi.IntParameter()

    def requires(self):
        return [KNN(cluster_id=str(i)) for i in
                range(self.num_clusters)]


if __name__ == '__main__':
    luigi.run(['TrainingManager', '--workers', '1', '--local-scheduler'])
