#!/usr/bin/python2

from flask import Flask, request, Response
import ConfigParser
import logging
import json
import requests
import threading

from get_data import get_anomalies_for_pkg
from get_data import get_type_app

logging.basicConfig(filename='smapper-rest.log',level=logging.INFO)

## Config file parsing to get values
Config = ConfigParser.ConfigParser()
Config.read('luigi.cfg')
listening_port = Config.get('SmapperRestService', 'listening_port')
FE_IP = Config.get('SmapperRestService', 'FE_IP')
FE_port = Config.get('SmapperRestService', 'FE_port')


# Worker taking care of the rest request on permissions. 
def permissions_worker(params=None):
	def send_post(info):
	        #Specific data refuired by front end
		headers = {'content-type':'application/json', 'accept':'application/json'}
		requests.post('http://'+FE_IP+':'+ FE_port
		              +'/ImdeaCompletedAnalysisNotify',
		              data=json.dumps(info),headers=headers)
		return
		
	#Info is the data structure used to reply the client
	info = {'Hash': params['Hash'],
                'Permissions': [],
                'AppCategory': '',
 		'Result': ''
		}

	#If decription is empty
        if not params['Details']['Description']:
        	info['Result'] = 5
		#Should return Resul 5 + empty list of perms + empty
		#category
                logging.info('Empty description')
		return send_post(info)

	#If permissions list is empty 
	if len(params['Details']['Permissions']) == 0:
                info['AppCategory'] = get_type_app(params)
	       	info['Result'] = 1
                logging.info('Empty list of perms')
		#Should return empty anomalous perms list + valid category
		return send_post(info)

	info['Result'] = 1
        # get list of anomalies
        anomaly_list = get_anomalies_for_pkg(params)
        logging.info(anomaly_list['features'])
        info['Permissions'] = anomaly_list['features']

        # get category name
        info['AppCategory'] = get_type_app(params)

        return send_post(info)


""" Flask app to respond to frontend request
"""
app = Flask(__name__)
@app.route('/StartAnalysis', methods=['POST'])
def startAnalysis():
    if request.method == 'POST':
	#Get data from POST request
        data = json.loads(request.data)
	#Starts analysis with permissions
	t = threading.Thread(target=permissions_worker,
	                     kwargs={'params' : data})
	t.start()

	#Must indicate request was received
	data = { 'Result'  : 1 }
    	js = json.dumps(data)
    	resp = Response(js, status=200, mimetype='application/json')
    	return resp

    #Must indicate an error happened
    else:
	data = { 'Result' : 0, 'Error' : 'ERROR: Bad POST Request.' }
    	js = json.dumps(data)
    	resp = Response(js, status=200, mimetype='application/json')
    	return resp

# @app.route('/CompletionNotify', methods=['POST'])
# def completionNotify():
#     if request.method == 'POST':
# 	#Get data from POST request
#         data = json.loads(request.data)
# 	#Starts flow analysis with permissions
# 	t = threading.Thread(target=flow_analysis_worker, kwargs={'params' : data})
# 	t.start()

# 	#Must indicate request was received
# 	data = { 'Result'  : 1 }
#     	js = json.dumps(data)
#     	resp = Response(js, status=200, mimetype='application/json')
#     	return resp

#     else:
# 	#Must indicate an error happened
# 	data = { 'Result' : 0, 'Error' : 'ERROR: Bad POST Request.' }
#     	js = json.dumps(data)
#     	resp = Response(js, status=200, mimetype='application/json')
#     	return resp

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=int(listening_port), debug=True)
