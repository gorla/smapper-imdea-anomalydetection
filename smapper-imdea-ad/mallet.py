import json
import logging
import luigi
import os
import subprocess

from convert_to_txt import ConvertFilteredDescriptionToTXT
from convert_to_txt import ConvertFilteredDescriptionToTXTFromFolder

logger = logging.getLogger('luigi-interface')


'''The Mallet task reads all the preprocessed files in the input
folder, infers topics for each document, and produces a json file as
output that contains the set of topics --with corresponding
probabilities-- for each document.

'''
class Mallet(luigi.Task):

    num_topics = luigi.Parameter()
    topics_max = luigi.Parameter()
    topics_threshold = luigi.Parameter()

    mallet_path = luigi.Parameter()
    input_folder = luigi.Parameter()
    output_folder = luigi.Parameter()
    output_file = luigi.Parameter()
    
    binary_out_file = luigi.Parameter(default='topics_inf.mallet')
    inferencer_file = luigi.Parameter(default='inferencer')
    composition_file = luigi.Parameter(default='composition.txt')
    keywords_file = luigi.Parameter(default='keywords.txt')

    def requires(self):
        return ConvertFilteredDescriptionToTXTFromFolder()

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_folder,
                                              self.output_file))

    def run(self):
        # check if output folder exists. Create it otherwise.
        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

        logger.info('Importing data in Mallet format...')

        # to hide stdout and stderr
        FNULL = open(os.devnull, 'w')

        subprocess.call(["%s/mallet" % self.mallet_path,
                         "import-dir",
                         "--input", self.input_folder,
                         "--output", os.path.join(self.output_folder,
                                                  self.binary_out_file),
                         "--keep-sequence"], stdout=FNULL, stderr=FNULL)

        logger.info('Training Mallet topics model...')
        subprocess.call(["%s/mallet" % self.mallet_path,
                         "train-topics",
                         "--input", os.path.join(self.output_folder,
                                                 self.binary_out_file),
                         "--num-topics", self.num_topics,
                         "--output-topic-keys", os.path.join(self.output_folder,
                                                             self.keywords_file),
                         "--num-top-words", "200",
                         "--output-doc-topics", os.path.join(self.output_folder,
                                                             self.composition_file),
                         "--optimize-interval", "10",
                         "--doc-topics-max", self.topics_max,
                         "--doc-topics-threshold", self.topics_threshold,
                         "--inferencer-filename", os.path.join(self.output_folder,
                                                               self.inferencer_file)],
                        stdout=FNULL, stderr=FNULL)
        FNULL.close()

        # Read the doc-topics file and format it nicely in JSON for
        # output.
        apps_list_with_topics = {}
        with open(os.path.join(self.output_folder,
                               self.composition_file), 'r') as comp_fd:
            for line in comp_fd.readlines():
                # skip comment lines
                if line.startswith('#'):
                    continue
                l_tokens = line.strip().split()
                # Take the name of the app
                app_name = os.path.basename(l_tokens[1].replace('-details.json', ''))
                # we iterate through all topics and probabilities
                # (i.e. the rest of the line)
                topics_list = {}
                for index, token in enumerate(l_tokens[2:]):
                    if index % 2 == 0:
                        # index is the topic id add the tuple topic id
                        # + probability
                        topics_list[token] = l_tokens[index+3]
                # add the app with its topics and probs to the dict.
                apps_list_with_topics[app_name] = topics_list

        # Write dict to JSON format and save it to output file.
        with self.output().open('w') as f:
            f.write(json.dumps(apps_list_with_topics))

        logger.info('Done with Mallet...')


'''The MalletInferencer task infers topics for a new sample document,
and appends the new sample to the existing json file that contains the
set of topics --with corresponding probabilities-- for each document
analyzed so far.

'''
class MalletInferencer(luigi.Task):

    topics_max = luigi.Parameter()
    topics_threshold = luigi.Parameter()
    
    mallet_path = luigi.Parameter()
    input_file = luigi.Parameter()
    output_folder = luigi.Parameter()

    binary_out_file = luigi.Parameter(default='topics_inf.mallet')
    inferencer_file = luigi.Parameter(default='inferencer')
    inferenced_file = luigi.Parameter(default='inferenced.txt')
    output_file = luigi.Parameter()

    def requires(self):
        return ConvertFilteredDescriptionToTXT(json_file=self.input_file)

    def output(self):
        return luigi.LocalTarget(os.path.join(self.output_folder,
                                              self.output_file))

    def complete(self):
        app_name = os.path.basename(self.input_file.replace('-details.json', ''))
        if app_name in self.output().open('r').read():
            return True
        else:
            return False

    def run(self):
        # check if output folder exists. Create it otherwise.
        if not os.path.exists(self.output_folder):
            os.makedirs(self.output_folder)

        logger.info('Importing file in Mallet format...')

        # to hide stdout and stderr
        FNULL = open(os.devnull, 'w')
        subprocess.call(["%s/mallet" % self.mallet_path,
                         "import-file",
                         "--input", self.input_file,
                         "--output", os.path.join(self.output_folder,
                                                  self.binary_out_file),
                         "--keep-sequence"], stdout=FNULL, stderr=FNULL)

        logger.info('Inferring Mallet topics model for new sample ...')

        subprocess.call(["%s/mallet" % self.mallet_path,
                         "infer-topics", "--input",
                         os.path.join(self.output_folder,
                                      self.binary_out_file), "--inferencer",
                         os.path.join(self.output_folder,
                                      self.inferencer_file), "--doc-topics-max",
                         self.topics_max, "--doc-topics-threshold",
                         self.topics_threshold, "--output-doc-topics",
                         os.path.join(self.output_folder,
                                      self.inferenced_file)], stdout=FNULL,
                        stderr=FNULL)
        FNULL.close()

        # Read the doc-topics file and format it nicely in JSON for
        # output.
        with open(os.path.join(self.output_folder, self.output_file),
                  'r+') as out_f:
            apps_list_with_topics = json.loads(out_f.read())
            with open(os.path.join(self.output_folder,
                               self.inferenced_file), 'r') as inf_fd:
                for line in inf_fd.readlines():
                    # skip comment lines
                    if line.startswith('#'):
                        continue
                    l_tokens = line.strip().split()
                    # Take the name of the app
                    app_name = os.path.basename(self.input_file.replace('-details.json', ''))
                    # we iterate through all topics and probabilities
                    # (i.e. the rest of the line)
                    topics_list = {}
                    for index, token in enumerate(l_tokens[2:]):
                        if index % 2 == 0:
                            # then index is the topic id
                            # add the tuple topic id + probability
                            topics_list[token] = l_tokens[index+3]
                    # add the app with its topics and prob to the dict.
                    apps_list_with_topics[app_name] = topics_list

                # overwrite app topics file with new content 
                out_f.seek(0)
                out_f.write(json.dumps(apps_list_with_topics))
                out_f.truncate()
        #TODO: keep track that teh sample has been analyzed, but it
        #has not been considered for training.
        logger.info('Done with MalletInferencer....')
