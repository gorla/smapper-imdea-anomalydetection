CODE_FOLDER=smapper-imdea-ad
SAMPLE_FILE=com.whatsapp.wallpaper-details.json
FOLDER_JSON_TARGET=$(CODE_FOLDER)/data_test
FOLDER_JSON_SOURCE=$(CODE_FOLDER)/data_top100
NUM_TOPICS=$(shell seq 10 12)
NUM_CLUSTERS=$(shell seq 10 12)
NUM_TOPICS_PER_APP=$(shell seq 4 6)
SILHOUETTE_DIR=$(CODE_FOLDER)/silhouette
MALLET_FOLDER=mallet-2.0.6

PYTHON=python2.7

training:
	cd $(CODE_FOLDER)/ & \
	$(PYTHON) -m luigi --module chabada_model_training TrainingManager

inference:
	cp $(FOLDER_JSON_SOURCE)/$(SAMPLE_FILE) $(FOLDER_JSON_TARGET)/
	cd $(CODE_FOLDER)/ & \
	$(PYTHON) -m luigi --module chabada_analysis AnalysisManager --sample-file $(FOLDER_JSON_TARGET)/$(SAMPLE_FILE)

clean:
	rm -rf knn/ \
		$(FOLDER_JSON_TARGET)/$(SAMPLE_FILE) \
		$(CODE_FOLDER)/clusters/ \
		$(CODE_FOLDER)/descriptions/ \
		$(CODE_FOLDER)/mallet_out/ \
		$(CODE_FOLDER)/silhouette/

all_silhouette: silhouette $(foreach ntopics,$(NUM_TOPICS),$(foreach cl,$(NUM_CLUSTERS),$(foreach ntopicsapps,$(NUM_TOPICS_PER_APP),$(SILHOUETTE_DIR)/$(ntopics)-$(cl)-$(ntopicsapps).txt)))

silhouette:
	mkdir -p $@

# $(1) number of topics
# $(2) number of clusters
# $(3) number of topics per app
define SILHOUETTE_exec
$$(SILHOUETTE_DIR)/$(1)-$(2)-$(3).txt:
	cd $(CODE_FOLDER)/ & \
	$(PYTHON) -m luigi --module kmeans KmeansTraining \
	--silhouette-out $$@ \
	--num-clusters $(2) \
	--num-topics $(1) \
	--output-folder clusters-$(1)-$(2)-$(3) \
	--Mallet-num-topics $(1) \
	--Mallet-topics-max $(3) \
	--Mallet-output-folder mallet-out-$(1)-$(2)-$(3)
endef

$(foreach ntopics,$(NUM_TOPICS),$(foreach cl,$(NUM_CLUSTERS),$(foreach ntopicsapps,$(NUM_TOPICS_PER_APP),$(eval $(call SILHOUETTE_exec,$(ntopics),$(cl),$(ntopicsapps))))))


# download and install mallet
$(MALLET_FOLDER):
	wget http://mallet.cs.umass.edu/dist/mallet-2.0.6.tar.gz
	tar -zxvf mallet-2.0.6.tar.gz
	rm mallet-2.0.6.tar.gz
