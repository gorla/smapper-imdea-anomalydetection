# Setup

* Install dependencies by running `python2.7 -m pip install -r requirements.txt` (already done)
* Install nltk packages by running `python -m nltk.downloader stopwords punkt maxent_treebank_pos_tagger averaged_perceptron_tagger wordnet`
* Install mallet by running `make mallet-2.0.6`
* Uncompress the folders with the trained data in folder `smapper-imdea-ad`:
    * `cd smapper-imdea-ad; unzip top_100_playstore-details.zip; mv output data_top100`
    * `cd smapper-imdea-ad; tar -zxvf clusters.tar.gz`
    * `cd smapper-imdea-ad; tar -zxvf descriptions.tar.gz`
    * `cd smapper-imdea-ad; tar -zxvf mallet-out.tar.gz`
    * `cd smapper-imdea-ad; tar -zxvf knn.tar.gz`
* Create your own `luigi.cfg` config file in `smapper-imdea-ad`:
    * `cp luigi.cfg.skel luigi.cfg` (in principle all could stay the same)

# Start the service

* start `luigid` as a background process. This is the scheduler that decides how to schedule all the jobs.
* start the rest service (edit the `luigi.cfg` file if you want to change ip and port of the frontend server):
    * `python2.7 smapper-imdea-rest.py`

    
    
    
